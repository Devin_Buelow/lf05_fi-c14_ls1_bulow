	
public class Konfiguration_Chaos {

    public static void main(String[] args) {
        double maximum = 100.00;
        String typ = "Automat AVR";
        char sprachModul = 'd';

        boolean statusCheck;
        String bezeichnung = "Q2021_FAB_A";

        String name = typ + " " + bezeichnung;
        final byte PRUEFNR = 4;

        double patrone = 46.24;
        double fuellstand = maximum - patrone;
        int muenzenCent = 1280;
        int muenzenEuro = 130;

        int summe = muenzenCent + muenzenEuro * 100;
        int euro = summe / 100;
        int cent = summe % 100;

        statusCheck = (euro <= 150) && (fuellstand >= 50.00) && (!(PRUEFNR == 5 || PRUEFNR == 6)) && (euro >= 50)
                && (cent != 0) && (sprachModul == 'd');

        System.out.println("Name: " + name);
        System.out.println("Sprache: " + sprachModul);
        System.out.println("Pr�fnummer : " + PRUEFNR);
        System.out.println("F�llstand Patrone: " + fuellstand + " %");
        System.out.println("Summe Euro: " + euro + " Euro");
        System.out.println("Summe Rest: " + cent + " Cent");
        System.out.println("Status: " + statusCheck);
    }
}