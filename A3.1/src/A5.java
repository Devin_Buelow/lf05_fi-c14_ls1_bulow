
public class A5 {

    public static void main(String[] args) {
        System.out.println(convertArrayToString(new int[] { 1, 2, 3 }));
    }

    public static String convertArrayToString(int[] zahlen) {

        String convert = "";

        for (int i = 0; i < zahlen.length; i++) {
            convert = convert + zahlen[i] + ", ";
        }
        return convert.subSequence(0, convert.length() - 2).toString();
    }

}
